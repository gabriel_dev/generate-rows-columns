let data = [1,2,3,4,5,6,7,8,9,10];
let rows = [];
let dummyArray = [];

let lastKnowPosition = 0;
let size = 3;

function generate() {
    for (let i = lastKnowPosition; i < size; i++) {
        if (data[i] !== undefined) {
            dummyArray.push(data[i]);
        }
        if (i === (size - 1)) {
            lastKnowPosition = i + 1;
            size = lastKnowPosition + 3;
            break;
        }
    }
}

while(lastKnowPosition <= data.length) {
    generate();
    rows.push(dummyArray);
    dummyArray = [];
}

console.log(rows);